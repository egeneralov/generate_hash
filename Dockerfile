FROM python:3

WORKDIR /app/

RUN pip install pyinstaller

ADD requirements.txt /app/

RUN pip install --no-cache-dir -r /app/requirements.txt

ADD . .

RUN make


FROM debian:9

COPY --from=0 /app/app /

ENTRYPOINT ["/app"]
