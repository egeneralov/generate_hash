import argparse
import hashlib
import binascii


def generate(password, salt):
  password = password.encode()
  salt = salt.encode()
  dk = hashlib.pbkdf2_hmac(
    hash_name = 'sha1',
    password = password,
    salt = salt,
    iterations = 1,
    dklen = 16
  )
  hash = binascii.hexlify(dk)
  return hash.decode()


if __name__ == "__main__":
  parser = argparse.ArgumentParser(
    description='Generate pbkdf2 hmac sha1'
  )
  
  parser.add_argument(
    'password',
    type = str,
    help = 'string to hash'
  )
  
  parser.add_argument(
    '--salt',
    type = str,
    default = "a123()12`12-3_)+~=-sd-12asASjdhfk",
    help = 'salt'
  )

  args = parser.parse_args()
  print(
    generate(
      password = args.password,
      salt = args.salt
    )
  )
