app:
	pyinstaller --distpath . --console --onefile --name app app.py

clean:
	find . -name \*.pyc -type f | xargs rm -f
	find . -name __pycache__ -type d | xargs rm -rf
	rm -rf app.spec build app
